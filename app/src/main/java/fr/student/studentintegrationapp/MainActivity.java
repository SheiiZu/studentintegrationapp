package fr.student.studentintegrationapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.text.BreakIterator;
import java.util.List;

import fr.student.studentintegrationapp.databinding.ActivityMainBinding;
import fr.student.studentintegrationapp.ui.home.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fr.student.studentintegrationapp.databinding.ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);
        binding.appBarMain.fab.setOnClickListener(view -> {
            String url = "mailto:international@3il.fr";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_steps, R.id.nav_housing,
                R.id.nav_financial, R.id.nav_language, R.id.nav_places,
                R.id.nav_culture, R.id.nav_transport)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void modifierTexte(String localisationTxt, TextView textView) {
        // Accédez à la ressource de chaîne dans strings.xml
        int resId = getResources().getIdentifier(localisationTxt, "string", getPackageName());

        textView.setText(localisationTxt);
    }

    public void modifierLangue(String langue, Fragment fragment) {
        String fragmentName = fragment.getClass().getSimpleName();

        switch (langue) {
            case "fr":
                switch (fragmentName) {
                    case "CultureFragment":
                        // Gérer le fragment CultureFragment pour la langue française
                        modifierTexte(getString(R.string.culture_text), findViewById(R.id.textViewCulture));
                        break;
                    case "FinancialFragment":
                        // Gérer le fragment FinancialFragment pour la langue française
                        modifierTexte(getString(R.string.financial_text), fragment.requireView().findViewById(R.id.textViewFinancial));
                        break;
                    case "HomeFragment":
                        // Gérer le fragment HomeFragment pour la langue française
                        modifierTexte(getString(R.string.home_text), fragment.requireView().findViewById(R.id.textViewHome));
                        break;
                    case "HousingFragment":
                        // Gérer le fragment HousingFragment pour la langue française
                        modifierTexte(getString(R.string.housing_text), fragment.requireView().findViewById(R.id.textViewHousing));
                        break;
                    case "LanguageFragment":
                        // Gérer le fragment LanguageFragment pour la langue française
                        modifierTexte(getString(R.string.language_text), fragment.requireView().findViewById(R.id.textViewLanguage));
                        break;
                    case "PlacesFragment":
                        // Gérer le fragment PlacesFragment pour la langue française
                        modifierTexte(getString(R.string.places_text), fragment.requireView().findViewById(R.id.textViewPlaces));
                        break;
                    case "StepsFragment":
                        // Gérer le fragment StepsFragment pour la langue française
                        modifierTexte(getString(R.string.steps_text), fragment.requireView().findViewById(R.id.textViewSteps));
                        break;
                    case "TransportFragment":
                        // Gérer le fragment TransportFragment pour la langue française
                        modifierTexte(getString(R.string.transport_text), fragment.requireView().findViewById(R.id.textViewTransport));
                        break;
                    default:
                        throw new IllegalArgumentException("Fragment non reconnu pour la langue française: " + fragmentName);
                }
                break;

            case "en":
                switch (fragmentName) {
                    case "CultureFragment":
                        // Gérer le fragment CultureFragment pour la langue anglaise
                        modifierTexte(getString(R.string.culture_text_en), findViewById(R.id.textViewCulture));
                        break;
                    case "FinancialFragment":
                        // Gérer le fragment FinancialFragment pour la langue anglaise
                        modifierTexte(getString(R.string.financial_text_en), fragment.requireView().findViewById(R.id.textViewFinancial));
                        break;
                    case "HomeFragment":
                        // Gérer le fragment HomeFragment pour la langue anglaise
                        modifierTexte(getString(R.string.home_text_en), fragment.requireView().findViewById(R.id.textViewHome));
                        break;
                    case "HousingFragment":
                        // Gérer le fragment HousingFragment pour la langue anglaise
                        modifierTexte(getString(R.string.housing_text_en), fragment.requireView().findViewById(R.id.textViewHousing));
                        break;
                    case "LanguageFragment":
                        // Gérer le fragment LanguageFragment pour la langue anglaise
                        modifierTexte(getString(R.string.language_text_en), fragment.requireView().findViewById(R.id.textViewLanguage));
                        break;
                    case "PlacesFragment":
                        // Gérer le fragment PlacesFragment pour la langue anglaise
                        modifierTexte(getString(R.string.places_text_en), fragment.requireView().findViewById(R.id.textViewPlaces));
                        break;
                    case "StepsFragment":
                        // Gérer le fragment StepsFragment pour la langue anglaise
                        modifierTexte(getString(R.string.steps_text_en), fragment.requireView().findViewById(R.id.textViewSteps));
                        break;
                    case "TransportFragment":
                        // Gérer le fragment TransportFragment pour la langue anglaise
                        modifierTexte(getString(R.string.transport_text_en), fragment.requireView().findViewById(R.id.textViewTransport));
                        break;
                    default:
                        throw new IllegalArgumentException("Fragment non reconnu pour la langue anglaise: " + fragmentName);
                }
                break;

            case "es":
                switch (fragmentName) {
                    case "CultureFragment":
                        // Gérer le fragment CultureFragment pour la langue espagnole
                        modifierTexte(getString(R.string.culture_text_es), findViewById(R.id.textViewCulture));
                        break;
                    case "FinancialFragment":
                        // Gérer le fragment FinancialFragment pour la langue espagnole
                        modifierTexte(getString(R.string.financial_text_es), fragment.requireView().findViewById(R.id.textViewFinancial));
                        break;
                    case "HomeFragment":
                        // Gérer le fragment HomeFragment pour la langue espagnole
                        modifierTexte(getString(R.string.home_text_es), fragment.requireView().findViewById(R.id.textViewHome));
                        break;
                    case "HousingFragment":
                        // Gérer le fragment HousingFragment pour la langue espagnole
                        modifierTexte(getString(R.string.housing_text_es), fragment.requireView().findViewById(R.id.textViewHousing));
                        break;
                    case "LanguageFragment":
                        // Gérer le fragment LanguageFragment pour la langue espagnole
                        modifierTexte(getString(R.string.language_text_es), fragment.requireView().findViewById(R.id.textViewLanguage));
                        break;
                    case "PlacesFragment":
                        // Gérer le fragment PlacesFragment pour la langue espagnole
                        modifierTexte(getString(R.string.places_text_es), fragment.requireView().findViewById(R.id.textViewPlaces));
                        break;
                    case "StepsFragment":
                        // Gérer le fragment StepsFragment pour la langue espagnole
                        modifierTexte(getString(R.string.steps_text_es), fragment.requireView().findViewById(R.id.textViewSteps));
                        break;
                    case "TransportFragment":
                        // Gérer le fragment TransportFragment pour la langue espagnole
                        modifierTexte(getString(R.string.transport_text_es), fragment.requireView().findViewById(R.id.textViewTransport));
                        break;
                    default:
                        throw new IllegalArgumentException("Fragment non reconnu pour la langue espagnole: " + fragmentName);
                }
                break;

            case "ru":
                switch (fragmentName) {
                    case "CultureFragment":
                        // Gérer le fragment CultureFragment pour la langue russe
                        modifierTexte(getString(R.string.culture_text_ru), findViewById(R.id.textViewCulture));
                        break;
                    case "FinancialFragment":
                        // Gérer le fragment FinancialFragment pour la langue russe
                        modifierTexte(getString(R.string.financial_text_ru), fragment.requireView().findViewById(R.id.textViewFinancial));
                        break;
                    case "HomeFragment":
                        // Gérer le fragment HomeFragment pour la langue russe
                        modifierTexte(getString(R.string.home_text_ru), fragment.requireView().findViewById(R.id.textViewHome));
                        break;
                    case "HousingFragment":
                        // Gérer le fragment HousingFragment pour la langue russe
                        modifierTexte(getString(R.string.housing_text_ru), fragment.requireView().findViewById(R.id.textViewHousing));
                        break;
                    case "LanguageFragment":
                        // Gérer le fragment LanguageFragment pour la langue russe
                        modifierTexte(getString(R.string.language_text_ru), fragment.requireView().findViewById(R.id.textViewLanguage));
                        break;
                    case "PlacesFragment":
                        // Gérer le fragment PlacesFragment pour la langue russe
                        modifierTexte(getString(R.string.places_text_ru), fragment.requireView().findViewById(R.id.textViewPlaces));
                        break;
                    case "StepsFragment":
                        // Gérer le fragment StepsFragment pour la langue russe
                        modifierTexte(getString(R.string.steps_text_ru), fragment.requireView().findViewById(R.id.textViewSteps));
                        break;
                    case "TransportFragment":
                        // Gérer le fragment TransportFragment pour la langue russe
                        modifierTexte(getString(R.string.transport_text_ru), fragment.requireView().findViewById(R.id.textViewTransport));
                        break;
                    default:
                        throw new IllegalArgumentException("Fragment non reconnu pour la langue russe: " + fragmentName);
                }
                break;

            default:
                throw new IllegalArgumentException("Langue non reconnue: " + langue);
        }
    }

    // Méthode pour récupérer le fragment actuellement affiché
    private Fragment getFragmentActuel() {
        // Obtenez le gestionnaire de fragments du NavHostFragment dans le content_main
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment navHostFragment = fragmentManager.findFragmentById(R.id.nav_host_fragment_content_main);

        if (navHostFragment != null) {
            // Obtenez le fragment actuellement affiché dans le NavHostFragment
            Fragment fragmentActuel = navHostFragment.getChildFragmentManager().getFragments().get(0);
            return fragmentActuel;
        } else {
            return null; // Aucun fragment trouvé
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_fr) {
            modifierLangue("fr", getFragmentActuel());
            return true;
        } else if (itemId == R.id.action_en) {// Appeler votre fonction pour l'élément action_en
            modifierLangue("en", getFragmentActuel());
            return true;
        } else if (itemId == R.id.action_es) {// Appeler votre fonction pour l'élément action_es
            modifierLangue("es", getFragmentActuel());
            return true;
        } else if (itemId == R.id.action_ru) {// Appeler votre fonction pour l'élément action_ru
            modifierLangue("ru", getFragmentActuel());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}