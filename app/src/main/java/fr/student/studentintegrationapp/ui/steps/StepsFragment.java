package fr.student.studentintegrationapp.ui.steps;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import fr.student.studentintegrationapp.R;

public class StepsFragment extends Fragment {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_steps, container, false);

        // Trouver le TextView dans le layout pour afficher du texte
        TextView textView = view.findViewById(R.id.textViewSteps);

        // Activer les liens dans le TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }
}